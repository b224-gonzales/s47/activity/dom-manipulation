// alert("Hello Batch 224!");


// Activity Code
console.log(document.querySelector("#txt-first-name"));

console.log(document);

console.log(document.getElementById("txt-first-name"));

console.log(document.getElementsByClassName("txt-inputs"));

console.log(document.getElementsByTagName("input"));

// Event and Event Listeners

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
console.log(txtFirstName);
console.log(spanFullName);

txtFirstName.addEventListener("keyup", (event) => {
  spanFullName.innerHTML = (`${txtFirstName.value}, ${txtLastName.value}`);
});

txtLastName.addEventListener("keyup", (event) => {
  spanFullName.innerHTML = (`${txtFirstName.value}, ${txtLastName.value}`);
});

txtFirstName.addEventListener("keyup", printFirstName);
txtLastName.addEventListener("keyup", printLastName);

function printFirstName(event) {
  spanFullName.innerHTML = (`${txtFirstName.value}, ${txtLastName.value}`);
}

function printLastName(event) {
  spanFullName.innerHTML = (`${txtFirstName.value}, ${txtLastName.value}`);
}
// We can add multiple events in an element

// We can add multiple events in an element
txtFirstName.addEventListener("keyup", (event) => {
  console.log(event);
  console.log(event.target);
  console.log(event.target.value);
});

const labelFirstName = document.querySelector("#label-first-name");
console.log(labelFirstName);

labelFirstName.addEventListener("click", (e) => {
  console.log(e);
  console.log(e.target);
  alert("You clicked the First Name label.");
});

txtLastName.addEventListener("keyup", (event) => {
  console.log(event);
  console.log(event.target);
  console.log(event.target.value);
});

const labelLastName = document.querySelector("#label-last-name");
console.log(labelFirstName);

labelLastName.addEventListener("click", (e) => {
  console.log(e);
  console.log(e.target);
  alert("You clicked the Last Name label.");
});
